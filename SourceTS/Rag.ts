﻿import {RagTag} from "./Tags/RagTag"
import {PropertyTag} from "./Tags/PropertyTag"
import { ImpactInteractionModel } from "./ImpactArea/ImpactInteractionModel";
import { ImpactTag } from "./Tags/ImpactTags";

export class Rag {
    constructor(...tags: RagTag[]) {
        this.Properties = new Array<[PropertyTag, any]>();
        this.Tags = tags;
    }

    public Tags: Array<RagTag>;
    public Properties: Array<[PropertyTag, any]>;
    public ImpactInteractions : Array<ImpactRestrict>;

    public FindImpactRestrict(tag: ImpactTag):ImpactRestrict {
        let result = this.ImpactInteractions.find(ii => ii.Tag.Name === tag.Name)
        return result;
    }
}

export class ImpactRestrict{
    constructor(public Tag: ImpactTag, public  Interaction: ImpactInteractionModel){}
}