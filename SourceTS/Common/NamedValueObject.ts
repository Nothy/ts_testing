import { NamedObject } from "./NamedObject";

export class NamedValueObject<T> extends NamedObject{
    constructor(public Name: string, public value: T) { 
        super(Name);
    }
}