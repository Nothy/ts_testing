﻿import {PropertyTag} from "../Tags/PropertyTag"
import {RagTag} from "../Tags/RagTag"
import {AbstractConstructionPoint} from "../ConstructionArea/AbstractConstructionPoint"
import {UnitBaseFactory} from "./UnitBaseFactory"
import {UnitHumanoid} from "../UnitArea/UnitHumanoid"
import {UnitHumanoidPants} from "../UnitArea/UnitHumanoidPants"
import {Rag} from "../Rag"

export abstract class UnitFactory {
    public static PantsLeatherProduce(): UnitHumanoidPants {

        let result = UnitBaseFactory.ProduceHumanoidPants();

        this.produceLeather(result.Pelvis, 1);
        this.produceLeather(result.RightHip, 1);
        this.produceLeather(result.RightCalf, 1);
        this.produceLeather(result.LeftHip, 1);
        this.produceLeather(result.LeftCalf, 1);

        return result;

    }

    public static PersonProduce(): UnitHumanoid {
        let person = UnitBaseFactory.ProduceHumanoid();

        this.produceHead(person.Head, 1, 1);
        this.produceChest(person.Chest, 1, 1);
        this.produceStomach(person.Stomach, 1, 1);

        this.produceLimb(person.Pelvis, 1, 1);

        this.produceLimb(person.LeftShoulders, 1, 1);
        this.produceLimb(person.LeftUpperArm, 1, 1);
        this.produceLimb(person.LeftForeArm, 1, 1);
        this.produceLimb(person.LeftPalm, 1, 1);
        this.produceLimb(person.LeftHip, 1, 1);
        this.produceLimb(person.LeftCalf, 1, 1);
        this.produceLimb(person.LeftFoot, 1, 1);

        this.produceLimb(person.RightShoulders, 1, 1);
        this.produceLimb(person.RightUpperArm, 1, 1);
        this.produceLimb(person.RightForeArm, 1, 1);
        this.produceLimb(person.RightPalm, 1, 1);
        this.produceLimb(person.RightHip, 1, 1);
        this.produceLimb(person.RightCalf, 1, 1);
        this.produceLimb(person.RightFoot, 1, 1);

        return person;
    }

    private static produceLeather(abstractPoint: AbstractConstructionPoint, weight: number) {

        let rag = new Rag(RagTag.Clothes.Leather);

        rag.Properties.push([PropertyTag.PhysicTags.Weight, weight]);
        abstractPoint.PutOnRags(rag);

    }

    private static produceLimb(abstractPoint: AbstractConstructionPoint, weight: number, size: number): void{
        let skin = new Rag(RagTag.CreatureBody.Skin);
        skin.Properties.push([PropertyTag.PhysicTags.Weight, weight / 10]);
        skin.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let muscle = new Rag(RagTag.CreatureBody.Muscle);
        muscle.Properties.push([PropertyTag.PhysicTags.Weight, weight / 2]);
        muscle.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let bone = new Rag(RagTag.CreatureBody.Bone);
        bone.Properties.push([PropertyTag.PhysicTags.Weight, weight / 2]);
        bone.Properties.push([PropertyTag.PhysicTags.Size, size]);

        abstractPoint.PutOnRags(bone, muscle, skin);
    }

    private static produceStomach(abstractPoint: AbstractConstructionPoint, weight: number, size: number): void {
        let skin = new Rag(RagTag.CreatureBody.Skin);
        skin.Properties.push([PropertyTag.PhysicTags.Weight, 1]);
        skin.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let muscle = new Rag(RagTag.CreatureBody.Muscle);
        muscle.Properties.push([PropertyTag.PhysicTags.Weight, weight / 4]);
        muscle.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let gut = new Rag(RagTag.CreatureBody.Gut);
        gut.Properties.push([PropertyTag.PhysicTags.Weight, weight / 4]);
        gut.Properties.push([PropertyTag.PhysicTags.Size, size]);

        abstractPoint.PutOnRags(gut, muscle, skin);
    }

    private static produceChest(abstractPoint: AbstractConstructionPoint, weight: number, size: number): void {

        let skin = new Rag(RagTag.CreatureBody.Skin);
        skin.Properties.push([PropertyTag.PhysicTags.Weight, 1]);
        skin.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let muscle = new Rag(RagTag.CreatureBody.Muscle);
        muscle.Properties.push([PropertyTag.PhysicTags.Weight, weight / 4]);
        muscle.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let bone = new Rag(RagTag.CreatureBody.Bone);
        bone.Properties.push([PropertyTag.PhysicTags.Weight, weight / 4]);
        bone.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let heart = new Rag(RagTag.CreatureBody.Heart);
        heart.Properties.push([PropertyTag.PhysicTags.Weight, weight / 8]);
        heart.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let lung = new Rag(RagTag.CreatureBody.Lung);
        lung.Properties.push([PropertyTag.PhysicTags.Weight, weight / 8]);
        lung.Properties.push([PropertyTag.PhysicTags.Size, size]);

        abstractPoint.PutOnRags(heart, lung, bone, muscle, skin);

    }

    private static produceHead(abstractPoint : AbstractConstructionPoint, weight: number, size: number): void {
        let skin = new Rag(RagTag.CreatureBody.Skin);
        skin.Properties.push([PropertyTag.PhysicTags.Weight, 1]);
        skin.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let muscle = new Rag(RagTag.CreatureBody.Muscle);
        muscle.Properties.push([PropertyTag.PhysicTags.Weight, weight / 8]);
        muscle.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let bone = new Rag(RagTag.CreatureBody.Bone);
        bone.Properties.push([PropertyTag.PhysicTags.Weight, weight / 4]);
        bone.Properties.push([PropertyTag.PhysicTags.Size, size]);

        let brain = new Rag(RagTag.CreatureBody.Brain);
        brain.Properties.push([PropertyTag.PhysicTags.Weight, weight * 3 / 4]);
        brain.Properties.push([PropertyTag.PhysicTags.Size, size]);

        abstractPoint.PutOnRags(brain, bone, muscle, skin);
    }
}