﻿import {AbstractConstructionPointFactory} from "./AbstractConstructionPointFactory"
import {UnitHumanoid} from "../UnitArea/UnitHumanoid"
import {UnitHumanoidPants} from "../UnitArea/UnitHumanoidPants"

export abstract class UnitBaseFactory {
    public static ProduceHumanoidPants(): UnitHumanoidPants {
        let pelvis = AbstractConstructionPointFactory.Humanoid.Pants.Pelvis();

        let leftHip = AbstractConstructionPointFactory.Humanoid.Pants.LeftHip();
        let leftCalf = AbstractConstructionPointFactory.Humanoid.Pants.LeftCalf();

        let rightHip = AbstractConstructionPointFactory.Humanoid.Pants.RightHip();
        let rightCalf = AbstractConstructionPointFactory.Humanoid.Pants.RightCalf();

        pelvis.JoinConstructionPoint(leftHip, 1);
        leftHip.JoinConstructionPoint(leftCalf, 1);

        pelvis.JoinConstructionPoint(rightHip, 1);
        rightHip.JoinConstructionPoint(rightCalf, 1);

        let result = new UnitHumanoidPants();

        result.Points.push(
            pelvis,
            leftHip,
            leftCalf,
            rightHip,
            rightCalf
        );

        result.Pelvis = pelvis;

        result.LeftHip = leftHip;
        result.LeftCalf = leftCalf;

        result.RightHip = rightHip;
        result.RightCalf = rightCalf;

        return result;
    }
    
    public static ProduceHumanoid(): UnitHumanoid {
        let head = AbstractConstructionPointFactory.Humanoid.Head();
        let chest = AbstractConstructionPointFactory.Humanoid.Chest();
        let stomach = AbstractConstructionPointFactory.Humanoid.Stomach();
        let pelvis = AbstractConstructionPointFactory.Humanoid.Pelvis();

        let leftShoulders = AbstractConstructionPointFactory.Humanoid.LeftShoulder();
        let leftUpperArm = AbstractConstructionPointFactory.Humanoid.LeftUpperArm();
        let leftForeArm = AbstractConstructionPointFactory.Humanoid.LeftForeArm();
        let leftPalm = AbstractConstructionPointFactory.Humanoid.LeftPalm();

        let rightShoulders = AbstractConstructionPointFactory.Humanoid.RightShoulder();
        let rightUpperArm = AbstractConstructionPointFactory.Humanoid.RightUpperArm();
        let rightForeArm = AbstractConstructionPointFactory.Humanoid.RightForeArm();
        let rightPalm = AbstractConstructionPointFactory.Humanoid.RightPalm();

        let leftHip = AbstractConstructionPointFactory.Humanoid.LeftHip();
        let leftCalf = AbstractConstructionPointFactory.Humanoid.LeftCalf();
        let leftFoot = AbstractConstructionPointFactory.Humanoid.LeftFoot();

        let rightHip = AbstractConstructionPointFactory.Humanoid.RightHip();
        let rightCalf = AbstractConstructionPointFactory.Humanoid.RightCalf();
        let rightFoot = AbstractConstructionPointFactory.Humanoid.RightFoot();
        
        head.JoinConstructionPoint(chest, 1);
        chest.JoinConstructionPoint(stomach, 1);
        stomach.JoinConstructionPoint(pelvis, 1);

        chest.JoinConstructionPoint(leftShoulders, 1);
        leftShoulders.JoinConstructionPoint(leftUpperArm, 1);
        leftUpperArm.JoinConstructionPoint(leftForeArm, 1);
        leftForeArm.JoinConstructionPoint(leftPalm, 1);

        chest.JoinConstructionPoint(rightShoulders, 1);
        rightShoulders.JoinConstructionPoint(rightUpperArm, 1);
        rightUpperArm.JoinConstructionPoint(rightForeArm, 1);
        rightForeArm.JoinConstructionPoint(rightPalm, 1);

        stomach.JoinConstructionPoint(pelvis, 1);

        pelvis.JoinConstructionPoint(leftHip, 1);
        leftHip.JoinConstructionPoint(leftCalf, 1);
        leftCalf.JoinConstructionPoint(leftFoot, 1);

        pelvis.JoinConstructionPoint(rightHip, 1);
        rightHip.JoinConstructionPoint(rightCalf, 1);
        rightCalf.JoinConstructionPoint(rightFoot, 1);

        let result = new UnitHumanoid();

        result.Points.push(
            head,
            chest,
            stomach,
            pelvis,
            leftShoulders,
            leftUpperArm,
            leftForeArm,
            leftPalm,
            rightShoulders,
            rightUpperArm,
            rightForeArm,
            rightPalm,
            leftHip,
            leftCalf,
            leftFoot,
            rightHip,
            rightCalf,
            rightFoot
        );

        result.Head = head;
        result.Chest = chest;
        result.Stomach = stomach;
        result.Pelvis = pelvis;

        result.LeftShoulders = leftShoulders;
        result.LeftUpperArm = leftUpperArm;
        result.LeftForeArm = leftForeArm;
        result.LeftPalm = leftPalm;

        result.RightShoulders = rightShoulders;
        result.RightUpperArm = rightUpperArm;
        result.RightForeArm = rightForeArm;
        result.RightPalm = rightPalm;

        result.LeftHip = leftHip;
        result.LeftCalf = leftCalf;
        result.LeftFoot = leftFoot;

        result.RightHip = rightHip;
        result.RightCalf = rightCalf;
        result.RightFoot = rightFoot;

        return result;
    }
}
