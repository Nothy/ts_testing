﻿import {SpotTag} from "../Tags/SpotTag"
import {AbstractConstructionPoint} from "../ConstructionArea/AbstractConstructionPoint"

export abstract class AbstractConstructionPointFactory {

    static Humanoid = {

        Head(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.Head);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Chest);

            return result;
        },

        LeftShoulder(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.LeftShoulder);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Chest);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftUpperArm);

            return result;
        },

        LeftUpperArm(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.LeftUpperArm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftShoulder);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftForeArm);

            return result;
        },

        LeftForeArm(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.LeftForeArm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftUpperArm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftPalm);

            return result;
        },

        LeftPalm(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.LeftPalm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftForeArm);

            return result;
        },

        RightShoulder(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.RightShoulder);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Chest);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightUpperArm);

            return result;
        },

        RightUpperArm(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.RightUpperArm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightShoulder);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightForeArm);

            return result;
        },

        RightForeArm(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.RightForeArm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightUpperArm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightPalm);

            return result;
        },

        RightPalm(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.RightPalm);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightForeArm);

            return result;
        },

        Chest(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.Chest);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Head);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightShoulder);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftShoulder);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Stomach);

            return result;
        },

        Stomach(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.Stomach);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Chest);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Pelvis);

            return result;
        },

        Pelvis(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.Pelvis);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Stomach);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftHip);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.RightHip);

            return result;
        },

        LeftHip(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.LeftHip);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Pelvis);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftCalf);

            return result;
        },

        LeftCalf(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.LeftCalf);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftHip);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftFoot);

            return result;
        },

        LeftFoot(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.LeftFoot);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftCalf);

            return result;
        },

        RightHip(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.RightHip);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.Pelvis);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftCalf);

            return result;
        },

        RightCalf(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.RightCalf);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftHip);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftFoot);

            return result;
        },

        RightFoot(): AbstractConstructionPoint {
            let result = new AbstractConstructionPoint();

            result.SpotTags.push(SpotTag.Humanoid.RightFoot);
            result.BesideSpotRestrictions.push(SpotTag.Humanoid.LeftCalf);

            return result;
        },

        Pants : {
            Pelvis(): AbstractConstructionPoint {
                let result = new AbstractConstructionPoint();
                result.SpotTags.push(SpotTag.HumanoidPants.Pelvis);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.LeftHip);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.RightHip);
                return result;
            },
    
            LeftHip(): AbstractConstructionPoint {
                let result = new AbstractConstructionPoint();
    
                result.SpotTags.push(SpotTag.HumanoidPants.LeftHip);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.Pelvis);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.LeftCalf);
    
                return result;
            },
    
            LeftCalf(): AbstractConstructionPoint {
                let result = new AbstractConstructionPoint();
                result.SpotTags.push(SpotTag.HumanoidPants.LeftCalf);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.LeftHip);
                return result;
            },
    
            RightHip(): AbstractConstructionPoint {
                let result = new AbstractConstructionPoint();
                result.SpotTags.push(SpotTag.HumanoidPants.RightHip);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.Pelvis);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.LeftCalf);
                return result;
            },
    
            RightCalf(): AbstractConstructionPoint {
                let result = new AbstractConstructionPoint();
                result.SpotTags.push(SpotTag.HumanoidPants.RightCalf);
                result.BesideSpotRestrictions.push(SpotTag.HumanoidPants.LeftHip);
                return result;
            },
        }
    }
}
