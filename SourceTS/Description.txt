U = Unit
AP = AbstractConstructionPoint
ST = SpotTag
R = Rag
RT = RagTag
PT = PropertyTag


UnitHumanoid: Unit
UnitHumanoidPants: Unit

Unit
 |
 |- AP
 |- AP
    |- ST
    |- ST
    | 
    |- R
    |- R
       |- RT
       |- RT
       |
       |- PT
       |- PT


AP Neighbours: 
________
AP [BesideConstructionJoints]
            | - AbstractPhysicalJoint - AP
            | - AbstractPhysicalJoint - AP
            ...
________


AP Pile:
________
  ...
   AP
PointIn
   |
   |
PointOut
   AP
PointIn
   |
   |
PointOut
   AP
  ...
________





