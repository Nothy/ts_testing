import { Queue } from 'queue-typescript';
import { ImpactModel } from './ImpactModel';

export abstract class ImpactQueue {
    private static impactsQueue: Queue<ImpactModel> = new Queue<ImpactModel>();

    public static TryDequeue(): [boolean, ImpactModel]{
        var result = ImpactQueue.impactsQueue.dequeue();
        var isExists = result != undefined;

        return [isExists, result];
    }

    public static Enqueue(item: ImpactModel): void{
        ImpactQueue.impactsQueue.enqueue(item);
    }
}