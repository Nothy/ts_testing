export class ImpactInteractionModel{
    public PersentReflect: number;
    public PersentPass: number;
    public PersentAbsorb: number;

    public PersentReflectMultiplier: number = 1;
    public PersentPassMultiplier: number = 1;
    public PersentAbsorbMultiplier: number = 1;

    public static DefaultInteractionModel : ImpactInteractionModel = {
        PersentReflect : 0,
        PersentPass : 0,
        PersentAbsorb : 1,
        PersentReflectMultiplier : 1,
        PersentPassMultiplier : 1,
        PersentAbsorbMultiplier : 1
    };
}
