import { AbstractConstructionPoint } from './../ConstructionArea/AbstractConstructionPoint';
import { Unit } from './../UnitArea/Unit';
import { NamedValueObject } from '../Common/NamedValueObject';
import { ImpactTagSquare, ImpactTagTemperature } from '../Tags/ImpactTags';

export class ImpactModel {
    constructor(
        target: Unit, 
        targetPoint: AbstractConstructionPoint, 
        )
    {
        this.Target = target;
        this.TargetPoint = targetPoint;
    }

    Target: Unit;
    TargetPoint: AbstractConstructionPoint
    
    ImpactSquare:NamedValueObject<ImpactTagSquare>;
    ImpactTemperature: NamedValueObject<ImpactTagTemperature>;
}