import { Rag } from './../Rag';
import { ImpactModel } from './ImpactModel';
import { ImpactQueue } from "./ImpactQueue";
import { ImpactInteractionModel } from './ImpactInteractionModel';
import { ImpactRestrict } from './../Rag';


class ImpactService{
    public Process() : void {
        var impactItem = ImpactQueue.TryDequeue();

        while(impactItem[0]){
            this.ProcessImpact(impactItem[1]);
            impactItem = ImpactQueue.TryDequeue();
        }
    }

    private ProcessImpact(impactItem : ImpactModel): void{
        var topAP = impactItem.TargetPoint.GetTopAP();
        var currentRugs = topAP.Rags;
        
        for(let i = currentRugs.length - 1; i >= 0; i--){
            let currentRug = currentRugs[i];

            if(impactItem.ImpactSquare || null !== null)
            {
                let currentImpactRestrict = currentRug.FindImpactRestrict(impactItem.ImpactSquare);

                let currentInteract : ImpactInteractionModel;

                if(currentImpactRestrict === undefined) 
                    currentInteract = ImpactInteractionModel.DefaultInteractionModel;
                else 
                    currentInteract = currentImpactRestrict.Interaction;
                
                currentInteract
            }
        }
    }
}