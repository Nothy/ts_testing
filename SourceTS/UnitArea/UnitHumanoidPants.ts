﻿import {AbstractConstructionPoint} from "../ConstructionArea/AbstractConstructionPoint"
import {Unit} from "./Unit"

export class UnitHumanoidPants extends Unit{

    Pelvis: AbstractConstructionPoint;

    LeftHip: AbstractConstructionPoint;
    LeftCalf: AbstractConstructionPoint;

    RightHip: AbstractConstructionPoint;
    RightCalf: AbstractConstructionPoint;
}