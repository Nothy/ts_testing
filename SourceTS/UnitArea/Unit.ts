﻿import {AbstractConstructionPoint} from "../ConstructionArea/AbstractConstructionPoint"
import {SpotTag} from "../Tags/SpotTag"

export class Unit {
    public Points = new Array<AbstractConstructionPoint>();

    AppropriateAPoint(tag: SpotTag): AbstractConstructionPoint {
        let result = this.Points.find(a => a.SpotTags.includes(tag)) || null;

        return result;
    }
}