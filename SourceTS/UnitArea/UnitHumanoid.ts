﻿import {AbstractConstructionPoint} from "../ConstructionArea/AbstractConstructionPoint"
import {Unit} from "./Unit"

export class UnitHumanoid extends Unit{

    Head: AbstractConstructionPoint;
    Chest: AbstractConstructionPoint;
    Stomach: AbstractConstructionPoint;
    Pelvis: AbstractConstructionPoint;

    LeftShoulders: AbstractConstructionPoint;
    LeftUpperArm: AbstractConstructionPoint;
    LeftForeArm: AbstractConstructionPoint;
    LeftPalm: AbstractConstructionPoint;

    RightShoulders: AbstractConstructionPoint;
    RightUpperArm: AbstractConstructionPoint;
    RightForeArm: AbstractConstructionPoint;
    RightPalm: AbstractConstructionPoint;

    LeftHip: AbstractConstructionPoint;
    LeftCalf: AbstractConstructionPoint;
    LeftFoot: AbstractConstructionPoint;

    RightHip: AbstractConstructionPoint;
    RightCalf: AbstractConstructionPoint;
    RightFoot: AbstractConstructionPoint;

}