﻿import { SpotTag } from "../Tags/SpotTag"
import { AbstractPhysicalJoint } from "../Joints/AbstractPhysicalJoint"
import { Rag } from "../Rag"

export class AbstractConstructionPoint {
    constructor() {
        this.BesideSpotRestrictions = [];
        this.BesideConstructionJoints = [];

        this.SpotTags = new Array<SpotTag>();
        this.Rags = new Array<Rag>();
    }

    public SpotTags: Array<SpotTag>;
    public BesideSpotRestrictions: Array<SpotTag>;

    public BesideConstructionJoints: Array<AbstractPhysicalJoint>;

    public PointOut: AbstractConstructionPoint;
    public PointIn: AbstractConstructionPoint;
    public Rags: Array<Rag>;

    // Remove link to in APoint
    public TakeOffThis() {
        if (this.PointIn)
            this.PointIn.PointOut = null;
        this.PointIn = null;
    }

    // Create link to in APoint
    public PutOnConstructionPoint(joinedPoint: AbstractConstructionPoint): boolean {
        if (!joinedPoint.SpotTags.some(st => this.SpotTags.includes(st))) 
        return false;

        this.PointOut = joinedPoint;
        joinedPoint.PointIn = this;

        return true;
    }

    // Link to rags array
    public PutOnRags(...rags: Array<Rag>): void {
        this.Rags.push(...rags);
    }

    // Create link-joint to beside APoint with power. Check beside restriction
    public JoinConstructionPoint(joinedPoint: AbstractConstructionPoint, power: number): boolean {

        if (!joinedPoint.BesideSpotRestrictions.some(s => this.SpotTags.includes(s)) ||
            !this.BesideSpotRestrictions.some(s => joinedPoint.SpotTags.includes(s)))
            return false;

        AbstractPhysicalJoint.Produce(this, joinedPoint, power);

        return true;
    }

    public GetTopAP ():AbstractConstructionPoint{
        let previewTop = this.PointOut;
        let nextTop = this.PointOut;

        while(true){
            if(nextTop || null === null)
                return previewTop

            previewTop = nextTop;
            nextTop = nextTop.PointOut;
        }
    }
}