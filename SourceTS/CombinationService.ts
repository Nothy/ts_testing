﻿import {Unit} from "./UnitArea/Unit"
import {AbstractConstructionPoint} from "./ConstructionArea/AbstractConstructionPoint"

export abstract class CombinationService {
    static PutOn(...units: Array<Unit>): boolean {

        let resultLinks = new Array<[AbstractConstructionPoint, AbstractConstructionPoint]>();

        for (let i = 1; i < units.length; i++) {
            let first = units[i - 1];
            let second = units[i];
            let intersectCount = 0;

            first.Points.forEach(fp => {
                let matchPoint = second.Points.find(sp => sp.SpotTags.some(st => fp.SpotTags.includes(st)));

                if (matchPoint === undefined)
                    return;

                intersectCount++;

                resultLinks.push([fp, matchPoint]);
            });

            if (intersectCount === 0)
                return false;
        }

        resultLinks.forEach(l => l[0].PutOnConstructionPoint(l[1]));
        
        return true;
    }
}