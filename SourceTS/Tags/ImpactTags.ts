import { NamedObject } from './../Common/NamedObject';

export class ImpactTag extends NamedObject{
    constructor(public Name: string) { 
        super(Name);
    }
}

export class ImpactTagSquare extends ImpactTag{

    public static None = new ImpactTagSquare("None");
    public static Pike = new ImpactTagSquare("Pike");
    public static SharpEdge = new ImpactTagSquare("SharpEdge");
    public static Stick = new ImpactTagSquare("Stick");
    public static Hammer = new ImpactTagSquare("Hammer");
    public static Avalanche = new ImpactTagSquare("Avalanche");
}

export class ImpactTagTemperature extends ImpactTag{

    public static None = new ImpactTagTemperature("None");
    public static Freeze = new ImpactTagTemperature("Freeze");
    public static Burn = new ImpactTagTemperature("Burn");
}