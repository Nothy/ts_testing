﻿export class PropertyTag {
    constructor(public Name: string) { }

    public static PhysicTags = {
        Weight: new PropertyTag("Weight"),
        Size: new PropertyTag("Size"),
        Height: new PropertyTag("Height"),
        Designation: new PropertyTag("Designation"),
        Strength: new PropertyTag("Strength"),
        Durability: new PropertyTag("Durability"),
    }

    public static PhysicMultiplierTags = {
        Weight: new PropertyTag("Weight"),
        Size: new PropertyTag("Size"),
        Height: new PropertyTag("Height"),
        Designation: new PropertyTag("Designation"),
        Strength: new PropertyTag("Strength"),
        Durability: new PropertyTag("Durability"),
    }

    public static ClothesTags = {

    }

    public static PersonTags = {
        PersonName: new PropertyTag("PersonName")
    }

    public static PersonSocialTags = {
        Occupation: new PropertyTag("Occupation")
    }
}