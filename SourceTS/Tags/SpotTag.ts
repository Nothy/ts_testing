﻿
export class SpotTag {
    constructor(public Name: string) { }

    static Humanoid = {
        Head: new SpotTag("Head"),
        RightShoulder: new SpotTag("RightShoulder"),
        LeftShoulder: new SpotTag("LeftShoulder"),
        RightUpperArm: new SpotTag("RightUpperArm"),
        LeftUpperArm: new SpotTag("LeftUpperArm"),
        RightForeArm: new SpotTag("RightForeArm"),
        LeftForeArm: new SpotTag("LeftForeArm"),
        RightPalm: new SpotTag("RightPalm"),
        LeftPalm: new SpotTag("LeftPalm"),
        Chest: new SpotTag("Chest"),
        Stomach: new SpotTag("Stomach"),
        Pelvis: new SpotTag("Pelvis"),
        RightHip: new SpotTag("RightHip"),
        LeftHip: new SpotTag("LeftHip"),
        RightCalf: new SpotTag("RightCalf"),
        LeftCalf: new SpotTag("LeftCalf"),
        RightFoot: new SpotTag("RightFoot"),
        LeftFoot: new SpotTag("LeftFoot"),

    }

    static HumanoidPants = {
        Pelvis: SpotTag.Humanoid.Pelvis,
        RightHip: SpotTag.Humanoid.RightHip,
        LeftHip: SpotTag.Humanoid.LeftHip,
        RightCalf: SpotTag.Humanoid.RightCalf,
        LeftCalf: SpotTag.Humanoid.LeftCalf,
    }
}