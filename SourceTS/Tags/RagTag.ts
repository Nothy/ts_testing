﻿export class RagTag {
    constructor(public Name: string) { }

    static Clothes = {
        Iron: new RagTag("Iron"),
        Leather: new RagTag("Leather"),
        Cloth: new RagTag("Cloth"),
    }

    static CreatureBody = {
        Skin: new RagTag("Skin"),
        Muscle: new RagTag("Muscle"),
        Meat: new RagTag("Meat"),
        Bone: new RagTag("Bone"),
        Heart: new RagTag("Heart"),
        Lung: new RagTag("Lung"),
        Gut: new RagTag("Gut"),
        Brain: new RagTag("Brain"),
    }
}