﻿import {AbstractConstructionPoint} from "../ConstructionArea/AbstractConstructionPoint"

export class AbstractPhysicalJoint {

    public Point1: AbstractConstructionPoint;
    public Point2: AbstractConstructionPoint;
    public Power: number;

    static Produce(Point1: AbstractConstructionPoint, Point2: AbstractConstructionPoint, Power: number): AbstractPhysicalJoint {

        let result = new AbstractPhysicalJoint();
        result.Point1 = Point1;
        result.Point2 = Point2;
        result.Power = Power;
        result.Point1.BesideConstructionJoints.push(result);
        result.Point2.BesideConstructionJoints.push(result);

        return result;
    }

    public GetNeighbor(point: AbstractConstructionPoint): AbstractConstructionPoint {
        if (this.Point1 === point)
            return this.Point2;
        else
            return this.Point1;
    }
}
